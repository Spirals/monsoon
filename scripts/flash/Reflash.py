import Monsoon.reflash as reflash
import Monsoon.HVPM as HVPM
import Monsoon.LVPM as LVPM
import time

######################################
# Reflash unit with USB Protocol firmware
######################################
"""
Mon = LVPM.Monsoon()
Mon.setup_usb()
Mon.resetToBootloader()
time.sleep(1)

Ref = reflash.bootloaderMonsoon()
Ref.setup_usb()
Header, Hex = Ref.getHeaderFromFWM('LVPM_RevE_Prot_1_Ver32.fwm')
if(Ref.verifyHeader(Header)):
    Ref.writeFlash(Hex)
Ref.resetToMainSection()
"""
######################################
# Reflash unit with USB protocal, automatic bootloader and restart
######################################

"""
# Mon = HVPM.Monsoon()
Mon = LVPM.Monsoon()
Mon.setup_usb()
Mon.resetToBootloader()
time.sleep(1)

Ref = reflash.bootloaderMonsoon()
Ref.setup_usb()
Header, Hex = Ref.getHeaderFromFWM('LVPM_RevE_Prot_1_Ver32.fwm')
if (Ref.verifyHeader(Header)):
    Ref.writeFlash(Hex)
Ref.resetToMainSection()

time.sleep(5)
Mon.setup_usb()
#Mon.setPowerUpCurrentLimit(4)
#Mon.setRunTimeCurrentLimit(4)
Mon.fillStatusPacket()
print("Unit number " + repr(Mon.getSerialNumber()) + " finished. New firmware revision: " + repr(Mon.statusPacket.firmwareVersion))
Mon.closeDevice()
"""


######################################
# Reflashing with last firmware
######################################
Mon = reflash.bootloaderMonsoon()
Mon.setup_usb()
Header, Hex = Mon.getHeaderFromFWM('../../firmwares/LVPM_RevE_Prot_1_Ver32.fwm')
if(Mon.verifyHeader(Header)):
    Mon.writeFlash(Hex)
Mon.resetToMainSection()

