import Monsoon.reflash as reflash

######################################
# Return to the serial protocol firmware
######################################

Mon = reflash.bootloaderMonsoon()
Mon.setup_usb()
Hex = Mon.getHexFile('../../firmwares/PM_RevD_Prot17_Ver20.hex')
Mon.writeFlash(Hex)
Mon.resetToMainSection()
