# Monsoon

## Flashing

You'll find in the ``scripts/flash`` folder two scripts: 
* ``Reset.py``, which flashes the probe with the default old firmware;
* ``Reflash.py``, which uses the last version of the firmware.

To flash the probe, you need to press the "Output Enable" little button while booting the probe up (the status light should be red instead of green).

You'll need sudo rights to run these scripts.

## Taking measures

Two ways exist to take measures with the Monsoon, either the official library or the Android Open Source Project script. 

### [Python library](https://github.com/msoon/PyMonsoon)

You need the last version of the software to use this one.

### AOSP script (scripts/monsoon.py)

Default firmware is required to use the AOSP script.
